#
orcus checkout version: 048d13a28db069969ff1c065f883f40a6a5c9c8e
https://gitlab.com/orcus/orcus/-/tree/048d13a28db069969ff1c065f883f40a6a5c9c8e

# build-env
Build environment for orcus which includes all its dependencies.
git clone --recurse-submodules git@gitlab.com:yhuang/build-env.git
docker image build -t orcus_centos7 .
docker run --name orcus_centos7 -itd orcus_centos7

## attach to docker container
