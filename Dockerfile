FROM centos:centos7.2.1511
COPY . /root/build-env
COPY ./cmake-3.19.1.tar.gz /root/cmake-3.19.1.tar.gz
RUN yum install -y wget
RUN wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
RUN yum install -y vim
RUN yum install -y gcc-gfortran.x86_64
RUN yum install -y centos-release-scl
RUN yum install -y devtoolset-8
RUN yum install -y make autoconf automake libtool pkgconfig git python3-devel SDL-devel
RUN yum install -y bzip2
RUN echo 
ENV PATH=/opt/rh/devtoolset-8/root/usr/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin \
MANPATH=/opt/rh/devtoolset-8/root/usr/share/man \
PERL5LIB=/opt/rh/devtoolset-8/root//usr/lib64/perl5/vendor_perl:/opt/rh/devtoolset-8/root/usr/lib/perl5:/opt/rh/devtoolset-8/root//usr/share/perl5/vendor_perl \ 
X_SCLS=devtoolset-8 \ 
PCP_DIR=/opt/rh/devtoolset-8/root \ 
LD_LIBRARY_PATH=/opt/rh/devtoolset-8/root/usr/lib64:/opt/rh/devtoolset-8/root/usr/lib:/opt/rh/devtoolset-8/root/usr/lib64/dyninst:/opt/rh/devtoolset-8/root/usr/lib/dyninst:/opt/rh/devtoolset-8/root/usr/lib64:/opt/rh/devtoolset-8/root/usr/lib:/usr/local/lib/:/usr/lib64 \
PATH=/opt/rh/devtoolset-8/root/usr/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin \ 
PYTHONPATH=/opt/rh/devtoolset-8/root/usr/lib64/python2.7/site-packages:/opt/rh/devtoolset-8/root/usr/lib/python2.7/site-packages \ 
PKG_CONFIG_PATH=/opt/rh/devtoolset-8/root/usr/lib64/pkgconfig \
INFOPATH=/opt/rh/devtoolset-8/root/usr/share/info \
CPLUS_INCLUDE_PATH=/usr/include/python3.6m/
#env above uses gfortran 8 instead of the default 4
# RUN cd /root/boost_1_73_0 && ./bootstrap.sh
RUN yum install -y http://repo.okay.com.mx/centos/7/x86_64/release/okay-release-1-1.noarch.rpm
RUN yum install -y boost166 boost166-devel
# without CPLUS_INCLUDE_PATH=/usr/include/python3.6m/  "./b2 install" will failed : fatal error: pyconfig.h: No such file or directory
# RUN cd /root/boost_1_73_0 && ./b2 install threading=multi link=shared
RUN yum install -y openssl-devel
RUN cd /root && tar zxvf cmake-3.* && cd cmake-3.* && ./bootstrap --prefix=/usr/local && make -j$(nproc) && make install
## install spdlog
RUN cd /root/build-env/spdlog && mkdir build && cd build && /usr/local/bin/cmake .. && make -j && make install
ENV PKG_CONFIG_PATH="/opt/rh/devtoolset-8/root/usr/lib64/pkgconfig:/usr/local/lib64/pkgconfig/"
RUN cd /root/build-env/gnu && ./build.sh


