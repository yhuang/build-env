#!/usr/bin/env bash

set -e

INSTALLDIR=$PWD/install
export PKG_CONFIG_PATH=$INSTALLDIR/share/pkgconfig:$INSTALLDIR/lib/pkgconfig:$PKG_CONFIG_PATH
export LD_LIBRARY_PATH=$INSTALLDIR/lib64:$INSTALLDIR/lib:$LD_LIBRARY_PATH
export CPPFLAGS="-march=native $CPPFLAGS"

pushd ../orcus
chmod 777 test/python/*
make check
popd